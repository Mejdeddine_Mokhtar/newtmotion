from numpy import array, sum
import copy
import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from numpy import array

class ParticleDynamics:
    def __init__(self,
                Particle,
                Integrator,
                ):
        self.Particle = Particle
        self.Integrator = Integrator # The Integrator object will do the actual numerics
        self.history = [copy.deepcopy(Particle)] # history saves all time steps of the particle dynamics
                
    def run_dynamics(self, nsteps, dt):
        """run particle dynamics for nsteps and use time step dt"""
        self.Integrator(nsteps, dt, self.Particle, self.history)
            
    def get_history(self):
        return self.history
    
    def get_particle(self):
        return self.particle
    
    def plot_trajectory(self):
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        x = array([p.get_position() for p in self.history])
        ax.plot(x[:,0], x[:,1], x[:,2], label='trajectory')
        ax.legend()
        return x

class Particle:
    def __init__(self,
                x0, # initial position
                v0, # initial velocity
                M,  # mass
                ):
        self.x = array(x0, dtype = float) #save input as numpy array
        self.v = array(v0, dtype = float) #save input as numpy array
        self.M = M

    def get_position(self):
        return self.x
    
    def get_velocity(self):
        return self.v

    def set_position(self, x):
        self.x = x
    
    def set_velocity(self, v):
        self.v = v

    def get_mass(self):
        return self.M
    
    def set_potential(self, potential):
        self.potential = potential

    def get_potential(self, potential):
        return potential

    def get_potential_energy(self):
        """calculate the kinetic energy of the particle"""
        return self.potential.get_potential_energy(self)
    
    def get_force(self):
        """calculate the kinetic energy of the particle"""
        return self.potential.get_force(self)

    
    def get_kinetic_energy(self):
        """calculate the kinetic energy of the particle"""
        return 0.5*self.M*sum(self.v**2)

    

    
class SurfaceGravitation:
    def __init__(self,
                 g, # gravitational acceleration)
                 ):
        self.g = g
    
    def get_potential_energy(self, particle):
        """return potential energy of a particle object"""
        return particle.get_mass()*self.g*particle.get_position()[2] # the z coordinate
        
    def get_force(self, particle):
        """return force acting on a particle object"""
        return particle.get_mass() * self.g * array([0,0,-1.0])

    
   
    
def Euler(nsteps, dt, Particle, history):
    x = Particle.get_position()
    v = Particle.get_velocity()
    for n in range(nsteps):
        F = Particle.get_force()
        v = v + F/Particle.get_mass()*dt
        x = x + v*dt
        Particle.set_position(x)
        Particle.set_velocity(v)
        history.append(copy.deepcopy(Particle))
    
