import matplotlib.pyplot as plt
from Dynamics_of_a_particle_in_an_external_potential import* 

shotputball = Particle(x0=[0,0,1.8],
                       v0=[5.,0,5.],
                       M=4, # 4 kg is mass of the shotput ball for women, 7.26 kg for men
                      )

# Let's initiate the external potential:
ext_potential = SurfaceGravitation(g=9.81)

# Connect shotput ball and potential:
shotputball.set_potential(ext_potential)

shotput_dyn = ParticleDynamics(shotputball, Euler)

shotput_dyn.run_dynamics(nsteps=5,dt=0.5)

# plot and get the trajectory:
x=shotput_dyn.plot_trajectory()

plt.plot(x[:,0],x[:,2],'.-')
plt.plot(x[:,0], 0*x[:,2])
plt.xlabel('position along x [m]')
plt.ylabel('position along z [m]')
plt.show()
