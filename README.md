# NewtMotion

NewtMotion is a Python library for the computation of the dynamics of particles with Newtonian motion. It is widely used in the academic community to show students the ability of computers to simulate real systems. 